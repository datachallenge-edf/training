import pandas as pd


def create_date_features(df):
    """[summary]
    
    Parameters:
    ----------
    df : {[type]}
        [description]
    """
    df['date'] = pd.to_datetime(df['date'], format='%d.%m.%Y')
    df['day'] = df['date'].dt.day
    df['day_week'] = df['date'].dt.dayofweek
    df['month'] = df['date'].dt.month
    df['year'] = df['date'].dt.year

    return df