import pandas as pd

def get_unique_shop_item(train, test, features=['shop_id', 'item_id']): 
    unique_shop_item = train[features].isin(test[features]).prod(axis=1)
    train_unique = train[unique_shop_item > 0]
    return train_unique